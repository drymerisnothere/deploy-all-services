# Registry de lectura
server {
    listen 80;
    server_name registry.cslabrecha.org;

    return 301 https://$server_name$request_uri;
}

map $upstream_http_docker_distribution_api_version $docker_distribution_api_version {
    '' 'registry/2.0';
}

server{
    listen 443;
    server_name registry.cslabrecha.org;
    access_log /var/log/nginx/registry.access.log;
    error_log /var/log/nginx/registry.error.log;

    ssl on;
    ssl_certificate  /etc/ssl/general.crt;
    ssl_certificate_key /etc/ssl/general.key;

    ssl_protocols TLSv1.1 TLSv1.2;
    ssl_ciphers 'EECDH+AESGCM:EDH+AESGCM:AES256+EECDH:AES256+EDH';

    ssl_prefer_server_ciphers on;
    ssl_session_cache shared:SSL:10m;

    location / {
        resolver 127.0.0.11;
	set $backend "ui-registry";
        proxy_pass http://$backend:8080;
    }

    client_max_body_size 0;

    location /v2/ {
        if ($http_user_agent ~ "^(docker\/1\.(3|4|5(?!\.[0-9]-dev))|Go ).*$" ) {
            return 404;
        }

        limit_except GET HEAD {
            auth_basic "Registry realm";
            auth_basic_user_file /etc/nginx/auth/registry.htpasswd;
        }

        add_header 'Docker-Distribution-Api-Version' $docker_distribution_api_version always;

        resolver 127.0.0.11;
        set $backend "docker-registry";

        proxy_pass                          http://$backend:5000;
        proxy_set_header  Host              $http_host;   # required for docker client's sake
        proxy_set_header  X-Real-IP         $remote_addr; # pass on real client's IP
        proxy_set_header  X-Forwarded-For   $proxy_add_x_forwarded_for;
        proxy_set_header  X-Forwarded-Proto $scheme;
        proxy_read_timeout                  900;
    }
}
