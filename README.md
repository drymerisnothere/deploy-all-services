# deploy-all-services

La rama master de este repositorio un playbook para instalar todos los servicios de la Brecha Digital.

# Como usarlo
```bash
# Ver tags
ansible-playbook main.yml --list-tags
# Usar siempre --skip-tag medehes
ansible-playbook main.yml --tags $loQueSea --ask-vault-pass --skip-tag medehes
```

Antes de usar, leer TODO.
